-- MySQL dump 10.13  Distrib 5.6.36, for Linux (x86_64)
--
-- Host: localhost    Database: private-argent
-- ------------------------------------------------------
-- Server version	5.6.36

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `wp_EWD_FEUP_Fields`
--

DROP TABLE IF EXISTS `wp_EWD_FEUP_Fields`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wp_EWD_FEUP_Fields` (
  `Field_ID` mediumint(9) NOT NULL AUTO_INCREMENT,
  `Field_Name` text,
  `Field_Description` text,
  `Field_Type` text,
  `Field_Options` text,
  `Field_Show_In_Admin` text,
  `Field_Show_In_Front_End` text,
  `Field_Required` text,
  `Field_Order` mediumint(9) NOT NULL DEFAULT '0',
  `Field_Date_Created` datetime DEFAULT '0000-00-00 00:00:00',
  `Field_Equivalent` text,
  `Level_Exclude_IDs` text,
  UNIQUE KEY `id` (`Field_ID`)
) ENGINE=MyISAM AUTO_INCREMENT=91 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wp_EWD_FEUP_Fields`
--

LOCK TABLES `wp_EWD_FEUP_Fields` WRITE;
/*!40000 ALTER TABLE `wp_EWD_FEUP_Fields` DISABLE KEYS */;
INSERT INTO `wp_EWD_FEUP_Fields` VALUES (21,'Name','User full name','text','','Yes','Yes','Yes',0,'2016-10-24 15:49:50','None',''),(5,'Designation','User role, position, title, etc','textarea','','Yes','Yes','No',1,'2016-10-12 16:54:36','None',''),(7,'Email','','text','','Yes','Yes','Yes',3,'2016-10-12 16:56:01','None',''),(8,'Address','First line of address.','textarea','','Yes','Yes','No',8,'2016-10-12 16:59:27','Address',''),(9,'City','','text','','Yes','Yes','No',9,'2016-10-12 17:07:17','City',''),(10,'State','','select','AL,AK,AZ,AR,CA,CO,CT,DE,FL,GA,HI,ID,IL,IN,IA,KS,KY,LA,ME,MD,MA,MI,MN,MS,MO,MT,NE,NV,NH,NJ,NM,NY,NC,ND,OH,OK,OR,PA,RI,SC,SD,TN,TX,UT,VT,VA,WA,WV,WI,WY','Yes','Yes','No',10,'2016-10-12 17:14:48','Province',''),(11,'ZIP','Zip code','text','','Yes','Yes','No',11,'2016-10-12 17:16:36','Postal_Code',''),(12,'Phone','','text','','Yes','Yes','No',2,'2016-10-12 17:17:11','Phone',''),(13,'Office','','select','BOARD MEMBERS,FLORIDA - LAKE MARY - HIGHLAND CAPITAL MANAGEMENT,LOUISIANA - RUSTON - MAIN OFFICE, LOUISIANA - RUSTON - SERVICE CENTER, LOUISIANA - RUSTON - ORIGIN BANK, LOUISIANA - ALEXANDRIA - RED RIVER BANK, LOUISIANA - BATON ROUGE, ARKANSAS - HOT SPRINGS, LOUISIANA - LAKE CHARLES, LOUISIANA - MINDEN, LOUISIANA - MONROE, LOUISIANA - SHREVEPORT, TENNESSEE - MEMPHIS, TENNESSEE - NASHVILLE, KENTUCKY - LOUISVILLE, PENNSYLVANIA, TEXAS - BEAUMONT, TEXAS - SAN ANTONIO - THE TRUST COMPANY, TEXAS - CORPUS CHRISTI - THE TRUST COMPANY, SOUTH CAROLINA - GREENVILLE, TENNESSEE - MEMPHIS - HIGHLAND CAPITAL MANAGEMENT, GEORGIA - ATLANTA, MISSISSIPPI - OXFORD, NORTH CAROLINA - CHARLOTTE, OKLAHOMA - OKLAHOMA CITY, ALABAMA -  BIRMINGHAM','Yes','Yes','Yes',7,'2016-10-14 12:59:44','None',''),(14,'Birthday Date','','date','','Yes','Yes','No',4,'2016-10-14 13:01:32','None',''),(15,'LinkedIn Address','','text','','Yes','Yes','No',6,'2016-10-14 13:03:32','None',''),(16,'Anniversary Date','','date','','Yes','Yes','No',5,'2016-10-14 13:03:55','None',''),(17,'Profile Picture','Please upload user profile picture','picture','','Yes','Yes','No',12,'2016-10-14 13:05:25','None',''),(18,'Biography','','textarea','','Yes','Yes','No',14,'2016-10-14 13:06:00','None',''),(19,'Education Information','','textarea','','Yes','Yes','No',13,'2016-10-14 13:08:25','None',''),(25,'Heritage Trust Board Meeting Agenda','','radio','YES,NO','Yes','No','No',17,'2016-10-27 13:36:52','None','a:1:{i:0;s:1:\"1\";}'),(26,'Argent Trust TN Trust Management Committee','','radio','YES,NO','Yes','Yes','No',20,'2016-10-27 14:05:06','None','a:1:{i:0;s:1:\"1\";}'),(27,'Argent Trust Management Committee','','radio','YES,NO','Yes','No','No',22,'2016-10-27 14:05:22','None','a:1:{i:0;s:1:\"1\";}'),(28,'Argent Trust TX Administration Committee','','radio','YES,NO','Yes','No','No',24,'2016-10-27 14:05:38','None','a:1:{i:0;s:1:\"1\";}'),(29,'Argent Stockholder Site','','radio','YES,NO','Yes','No','No',27,'2016-10-27 14:05:55','None','a:1:{i:0;s:1:\"1\";}'),(30,'Argent Trust TN ESOP Committee','','radio','YES,NO','Yes','No','No',29,'2016-10-27 14:06:17','None','a:1:{i:0;s:1:\"1\";}'),(31,'Argent Trust TX Board Meeting','','radio','YES,NO','Yes','No','No',33,'2016-10-27 14:06:29','None','a:1:{i:0;s:1:\"1\";}'),(32,'Managers Meeting','','radio','YES,NO','Yes','No','No',35,'2016-10-27 14:06:43','None','a:1:{i:0;s:1:\"1\";}'),(33,'The Trust Company Benefits Paperwork','','radio','YES,NO','Yes','No','No',37,'2016-10-27 14:06:57','None','a:1:{i:0;s:1:\"1\";}'),(34,'Argent Trust LA WEEKLY Admin','','radio','YES,NO','Yes','No','No',39,'2016-10-27 14:07:09','None','a:1:{i:0;s:1:\"1\";}'),(35,'The Trust Company Employee Paperwork','','radio','YES,NO','Yes','No','No',41,'2016-10-27 14:07:22','None','a:1:{i:0;s:1:\"1\";}'),(36,'New Employee Paperwork','','radio','YES,NO','Yes','No','No',43,'2016-10-27 14:07:34','None','a:1:{i:0;s:1:\"1\";}'),(37,'Argent Trust TN Administrative and Review Committee','','radio','YES,NO','Yes','No','No',45,'2016-10-27 14:07:50','None','a:1:{i:0;s:1:\"1\";}'),(38,'Argent Trust TN New Business Committee','','radio','YES,NO','Yes','No','No',47,'2016-10-27 14:08:05','None','a:1:{i:0;s:1:\"1\";}'),(39,'Argent Trust TN Investment Committee','','radio','YES,NO','Yes','No','No',49,'2016-10-27 14:08:17','None','a:1:{i:0;s:1:\"1\";}'),(40,'Argent Trust TX New Business Committee','','radio','YES,NO','Yes','No','No',51,'2016-10-27 14:08:31','None','a:1:{i:0;s:1:\"1\";}'),(42,'Argent Trust TX Investment Committee','','radio','YES,NO','Yes','No','No',54,'2016-10-27 14:08:55','None','a:1:{i:0;s:1:\"1\";}'),(43,'Argent Trust LA Administration Committee','','radio','YES,NO','Yes','No','No',56,'2016-10-27 14:09:08','None','a:1:{i:0;s:1:\"1\";}'),(55,'Heritage Trust Board Meeting Agenda Editor','','radio','YES,NO','Yes','Yes','No',18,'2017-01-30 18:05:18','None',''),(45,'Argent Master Audit Committee Meeting','','radio','YES,NO','Yes','No','No',58,'2016-10-27 14:09:30','None','a:1:{i:0;s:1:\"1\";}'),(46,'Argent Trust TN Board Meeting','','radio','YES,NO','Yes','No','No',64,'2016-10-27 14:09:42','None','a:1:{i:0;s:1:\"1\";}'),(47,'Argent Advisors Board Meeting','','radio','YES,NO','Yes','No','No',62,'2016-10-27 14:09:54','None','a:1:{i:0;s:1:\"1\";}'),(48,'Argent Financial Group Board Meeting','','radio','YES,NO','Yes','No','No',60,'2016-10-27 14:38:26','None','a:1:{i:0;s:1:\"1\";}'),(51,'Argent Trust TN Subcommittee','','radio','YES,NO','No','Yes','No',31,'2016-10-27 19:57:54','None','a:1:{i:0;s:1:\"1\";}'),(53,'Is Board Member Only','','radio','YES,NO','Yes','No','Yes',19,'2016-12-21 22:08:48','None',''),(56,'Argent Trust TN Trust Management Committee Editor','','radio','YES,NO','Yes','Yes','No',21,'2017-01-30 18:06:02','None',''),(57,'Argent Trust Management Committee Editor','','radio','YES,NO','Yes','Yes','No',23,'2017-01-30 18:06:27','None',''),(58,'Argent Trust TX Administration Committee Editor','','radio','YES,NO','Yes','Yes','No',25,'2017-01-30 18:08:41','None',''),(59,'ATT Year End 2015 Committee Editor','','radio','YES,NO','Yes','Yes','No',26,'2017-01-30 18:09:01','None',''),(60,'Argent Stockholder Site Editor','','radio','YES,NO','Yes','Yes','No',28,'2017-01-30 18:09:29','None',''),(61,'Argent Trust TN ESOP Committee Editor','','radio','YES,NO','Yes','Yes','No',30,'2017-01-30 18:09:53','None',''),(62,'Argent Trust TN Subcommittee Editor','','radio','YES,NO','Yes','Yes','No',32,'2017-01-30 18:10:29','None',''),(63,'Argent Trust TX Board Meeting Editor','','radio','YES,NO','Yes','Yes','No',34,'2017-01-30 18:10:57','None',''),(64,'Managers Meeting Editor','','radio','YES,NO','Yes','Yes','No',36,'2017-01-30 18:14:02','None',''),(65,'The Trust Company Benefits Paperwork Editor','','radio','YES,NO','Yes','Yes','No',38,'2017-01-30 19:54:35','None',''),(66,'Argent Trust LA WEEKLY Admin Editor','','radio','YES,NO','Yes','Yes','No',40,'2017-01-30 19:55:21','None',''),(67,'The Trust Company Employee Paperwork Editor','','radio','YES,NO','Yes','Yes','No',42,'2017-01-30 19:55:57','None',''),(68,'New Employee Paperwork Editor','','radio','YES,NO','Yes','Yes','No',44,'2017-01-30 19:56:14','None',''),(69,'Argent Trust TN Administrative and Review Committee Editor','','radio','YES,NO','Yes','Yes','No',46,'2017-01-30 19:56:45','None',''),(70,'Argent Trust TN New Business Committee Editor','','radio','YES,NO','Yes','Yes','No',48,'2017-01-30 19:57:15','None',''),(71,'Argent Trust TN Investment Committee Editor','','radio','YES,NO','Yes','Yes','No',50,'2017-01-30 19:57:37','None',''),(72,'Argent Trust TX New Business Committee Editor','','radio','YES,NO','Yes','Yes','No',52,'2017-01-30 19:57:54','None',''),(73,'Aarin Test Editor','','radio','YES,NO','Yes','No','No',53,'2017-01-30 19:58:11','None',''),(74,'Argent Trust TX Investment Committee Editor','','radio','YES,NO','Yes','Yes','No',55,'2017-01-30 19:58:35','None',''),(75,'Argent Trust LA Administration Committee Editor','','radio','YES,NO','Yes','Yes','No',57,'2017-01-30 19:58:56','None',''),(76,'Argent Master Audit Committee Meeting Editor','','radio','YES,NO','Yes','Yes','No',59,'2017-01-30 19:59:14','None',''),(77,'Argent Financial Group Board Meeting Editor','','radio','YES,NO','Yes','Yes','No',61,'2017-01-30 19:59:31','None',''),(78,'Argent Advisors Board Meeting Editor','','radio','YES,NO','Yes','Yes','No',63,'2017-01-30 19:59:52','None',''),(79,'Argent Trust TN Board Meeting Editor','','radio','YES,NO','Yes','Yes','No',65,'2017-01-30 20:00:10','None','');
/*!40000 ALTER TABLE `wp_EWD_FEUP_Fields` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-08-04 15:30:52
